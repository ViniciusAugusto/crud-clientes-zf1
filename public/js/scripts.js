$(document).ready(function(){
	
  //maskara para o formulario	
  $('.date').mask('00/00/0000');
  $('.cep').mask('00000-000');
  
  //busca o cep ao sair do campo
  $("#cep").blur(function(){
	 
	  //remove a maskara
	  var cep = $(this).val().replace("-","");
	  
	  $.get("https://viacep.com.br/ws/"+cep+"/json/",function(retorno){
		  
		  $("#endereco").val(retorno.logradouro);
		  $("#numero").val(retorno.complemento);
		  $("#bairro").val(retorno.bairro);
		  $("#cidade").val(retorno.localidade);
		  $("#estado").val(retorno.uf);
	  
	  });
  })
  
});