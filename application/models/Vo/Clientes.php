<?php

class Application_Model_Vo_Clientes extends Application_Model_DbTable_Clientes {
   
   private $id, $nome_completo, $data_nascimento, $sexo, $cep, $endereco, $numero, $complemento, $bairro, $cidade, $estado;
    
   public function getId(){
   	return $this->id;
   }
   
   public function setId($id){
   	$this->id = $id;
   }
   
   public function getNomeCompleto(){
   	return $this->nome_completo;
   }
   
   public function setNomeCompleto($nome_completo){
   	$this->nome_completo = $nome_completo;
   }
   
   public function getDataNascimento(){
   	return $this->data_nascimento;
   }
   
   public function setDataNascimento($data_nascimento){
   	$this->data_nascimento = $data_nascimento;
   }
   
   public function getSexo(){
   	return $this->sexo;
   }
   
   public function setSexo($sexo){
   	$this->sexo = $sexo;
   }
   
   public function getCep(){
   	return $this->cep;
   }
   
   public function setCep($cep){
   	$this->cep = $cep;
   }
   
   public function getEndereco(){
   	return $this->endereco;
   }
   
   public function setEndereco($endereco){
   	$this->endereco = $endereco;
   }
   
   public function getNumero(){
   	return $this->numero;
   }
   
   public function setNumero($numero){
   	$this->numero = $numero;
   }
   
   public function getComplemento(){
   	return $this->complemento;
   }
   
   public function setComplemento($complemento){
   	$this->complemento = $complemento;
   }
   
   public function getBairro(){
   	return $this->bairro;
   }
   
   public function setBairro($bairro){
   	$this->bairro = $bairro;
   }
   
   public function getCidade(){
   	return $this->cidade;
   }
   
   public function setCidade($cidade){
   	$this->cidade = $cidade;
   }
   
   public function getEstado(){
   	return $this->estado;
   }
   
   public function setEstado($estado){
   	$this->estado = $estado;
   }
 
}