<?php

class Application_Model_Clientes {

    public function apagar($id) {
    	
        $tab = new Application_Model_DbTable_Clientes();
        $tab->delete('id = '.$id);
        
        return true;
    }

    public function atualizar(Application_Model_Vo_Clientes $cliente) {
        $tab = new Application_Model_DbTable_Clientes();
        $tab->update(array(
        	'nome_completo'   => $cliente->getNomeCompleto(),
			'data_nascimento' => $cliente->getDataNascimento(),
        	'sexo'         	  => $cliente->getSexo(),
        	'cep'  			  => $cliente->getCep(),
        	'endereco'		  => $cliente->getEndereco(),
        	'numero'		  => $cliente->getNumero(),
        	'complemento'	  => $cliente->getComplemento(),
        	'bairro'  		  => $cliente->getBairro(),
        	'cidade'		  => $cliente->getCidade(),
        	'estado'		  => $cliente->getEstado()								
        ), 'id = '.$cliente->getId());
        
        return true;
    }

    public function salvar(Application_Model_Vo_Clientes $cliente) {
        $tab = new Application_Model_DbTable_Clientes();
        $tab->insert(array(
        	'nome_completo'   => $cliente->getNomeCompleto(),
			'data_nascimento' => $cliente->getDataNascimento(),
        	'sexo'         	  => $cliente->getSexo(),
        	'cep'  			  => $cliente->getCep(),
        	'endereco'		  => $cliente->getEndereco(),
        	'numero'		  => $cliente->getNumero(),
        	'complemento'	  => $cliente->getComplemento(),
        	'bairro'  		  => $cliente->getBairro(),
        	'cidade'		  => $cliente->getCidade(),
        	'estado'		  => $cliente->getEstado()	
        ));
        
        $cliente->setId($tab->getAdapter()->lastInsertId());
        
        
        return true;
    }

}
