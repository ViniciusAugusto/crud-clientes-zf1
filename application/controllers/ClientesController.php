<?php

/**
 * @author ViniciusAugustoCunha <viniciusaugustocunha@gmail.com>
 */
class ClientesController extends Zend_Controller_Action {

    public function indexAction() {
    	
    	//instancia a classe VO de clientes
        $tab = new Application_Model_Vo_Clientes();
        
        //busca todos os clientes por ordem alfabetica
        $clientes = $tab->fetchAll(NULL, 'nome_completo ASC')->toArray();
        
        $this->view->clientes = $clientes;
    }

    public function createAction() {
        
    	//instancia classe do formulario de clientes
    	$form = new Application_Form_Clientes();
    	
    	//envia para a view o formulario de clientes
    	$this->view->form = $form;
    	
    	//verifica se veio POST
    	if($this->getRequest()->isPost()){
    		
    		//pega os valores do POST
    		$post = $this->getAllParams();
    		
    		//valida o formulario
    		if($form->isValid($post)){
    			
    			//pega os dados do formulario preenchido e validado
    			$data = $form->getValues();
    			
    			//converte a data para formato YYYY-MM-dd
    			$date = new Zend_Date($data['data_nascimento']);
    			$dataNascimento = $date->get('YYYY-MM-dd');
    			    			
    			$vo = new Application_Model_Vo_Clientes();//instancia classe do VO
    			
    			//passa os dados do POST para o VO
    			$vo->setNomeCompleto($data['nome_completo']);
    			$vo->setDataNascimento($dataNascimento);
    			$vo->setSexo($data['sexo']);
    			$vo->setCep($data['cep']);
    			$vo->setEndereco($data['endereco']);
    			$vo->setNumero($data['numero']);
    			$vo->setComplemento($data['complemento']);
    			$vo->setBairro($data['bairro']);
    			$vo->setCidade($data['cidade']);
    			$vo->setEstado($data['estado']);
    			
    			$model = new Application_Model_Clientes();
    			$model->salvar($vo);//salva os dados pelo model
    			
    			//instancia helper de msg
    			$flash = $this->_helper->flashMessenger;
    			
    			//envia a msg
    			$flash->addMessage("Registro salvo");
    			
    			//volta para action index
    			$this->_helper->Redirector->gotoSimpleAndExit('index');
    			
    		}
    	}
    	
    }

    public function deleteAction() {
        
    	//pega os id do cleinte
    	$id = (int) $this->getParam("id");
    	
    	//instancia o model de cliente
    	$model = new Application_Model_Clientes();
    	
    	//instancia o helper de msg
    	$flash = $this->_helper->flashMessenger;
    	
    	//verifica se apagou
		try{
			$model->apagar($id);//envia o id para o model apagar
			$flash->addMessage("Registro apagado");//envia a msg para usuario
		}catch (Exception $e){
			$flash->addMessage($e->getMessage());//envia a msg de erro para o usuario
		}
		
		//volta para action index do controller
		$this->_helper->Redirector->gotoSimpleAndExit('index');
    	
    }

    public function updateAction() {
    	
    	//pega o id passado por GET
    	$id = (int) $this->getParam("id");
    	
    	//intancia ModelDbTable do cliente
    	$tab = new Application_Model_DbTable_Clientes();
    	
    	//busca o cliente pelo id
    	$cliente = $tab->fetchRow("id = $id");
    	
    	//verifica se o cliente existe
		if($cliente === NULL){
			
			$flash = $this->_helper->flashMessenger;
			
			$flash->addMessage("Registro nao existe");
			 
			$this->_helper->Redirector->gotoSimpleAndExit('index');
		}
		
		//converte a data para formato YYYY-MM-dd
		$date = new Zend_Date($cliente['data_nascimento']);
		$cliente['data_nascimento'] = $date->get('dd/MM/YYYY');
    	
		//instancia o formulario de clientes
    	$form = new Application_Form_Clientes();
    	
    	//manda o form para view
    	$this->view->form = $form;
    	 
    	//verifica se enviou o POST
    	if($this->getRequest()->isPost()){
    		
    		//pega os dados do POST
    		$post = $this->getAllParams();
    		
    		//valida o formulario
    		if($form->isValid($post)){
    			
    			//pega os campos validados pelo form
    			$data = $form->getValues();
    			 
    			//converte a data para formato YYYY-MM-dd
    			$date = new Zend_Date($data['data_nascimento']);
    			$dataNascimento = $date->get('YYYY-MM-dd');
    			
    			//instancia a classe do VO
    			$vo = new Application_Model_Vo_Clientes();
    			
    			//passa os dados do POST para o VO
    			$vo->setId($id);
    			$vo->setNomeCompleto($data['nome_completo']);
    			$vo->setDataNascimento($dataNascimento);
    			$vo->setSexo($data['sexo']);
    			$vo->setCep($data['cep']);
    			$vo->setEndereco($data['endereco']);
    			$vo->setNumero($data['numero']);
    			$vo->setComplemento($data['complemento']);
    			$vo->setBairro($data['bairro']);
    			$vo->setCidade($data['cidade']);
    			$vo->setEstado($data['estado']);
    			
    			 
    			$model = new Application_Model_Clientes();
    			$model->atualizar($vo);//passa o vo para o model atualizar
    			 
    			//instalcia o helper de msg
    			$flash = $this->_helper->flashMessenger;
    			 
    			//envia a msg
    			$flash->addMessage("Registro atualizado");
    			 
    			//volta para action index do controller
    			$this->_helper->Redirector->gotoSimpleAndExit('index');
    			 
    		}
    	}else{
    		//se nao foi enviado o form
    		$form->populate($cliente->toArray());//preenche o form com os dados do cliente
    	}
    }

}
