<?php

class Application_Form_Clientes extends Twitter_Bootstrap3_Form_Vertical {

    public function init() {
    	
    	//seta o method do form para post
        $this->setMethod('post');

        //monta o campo nome
        $nome =  new Zend_Form_Element_Text('nome_completo', array(
        	'label' => 'Nome Completo(*)',
            'placeholder' => 'Digite aqui o nome do cliente',
        	'class'   => 'focused form-control',
        	'required' => true
        ));
        
        //Adiciona o validador de no minimo 10 caracteres
        $nome->addValidator(new Zend_Validate_StringLength(array(
        		"min" => 10
        )));
        //adiciona filtro converter o nome do cliente para uppercase
        $nome->addFilter(new Zend_Filter_StringToUpper());
        //adiciona o campo ao formulario
        $this->addElement($nome);
        
        //monta o campo data_nascimento
        $data_nascimento =  new Zend_Form_Element_Text('data_nascimento', array(
        		'label' => 'Data de Nascimento(*)',
        		'placeholder' => 'Digite aqui a data de nascimento do cliente',
        		'class'   => 'form-control date',
        		'validators'  => array (
        				new Zend_Validate_Date(array('format' => 'dd/MM/yyyy'))
        		),
        		'required' => true
        ));
        
        //adiciona o campo ao formulario
        $this->addElement($data_nascimento);
        
        //monta o campo sexp
        $sexo = new Zend_Form_Element_Select('sexo',array(
        		'label' => 'Sexo(*)',
        		'class' => 'form-control',
        		'required' => true,
        		'multiOptions' => array("" => "Selecione uma Opção", "F" => "Feminino", "M" => "Masculino")
        ));
        
        //adiciona o campo ao formulario
        $this->addElement($sexo);
        //faz a validacao
        $sexo->addFilter(new Zend_Filter_Null());
        
        //monta o campo cep
        $cep =  new Zend_Form_Element_Text('cep', array(
        		'label' => 'CEP',
        		'placeholder' => 'Digite aqui o CEP',
        		'class'   => 'form-control cep',
        		'required' => false
        ));        
        //Adiciona o validador de no minimo 9 caracteres
        $cep->addValidator(new Zend_Validate_StringLength(array(
        		"min" => 9
        )));        
        //adiciona o campo ao formulario
        $this->addElement($cep);
        
        
        //monta o campo endereco
        $endereco =  new Zend_Form_Element_Text('endereco', array(
        		'label' => 'Endereco',
        		'placeholder' => 'Digite aqui o endereco',
        		'class'   => 'form-control',
        		'required' => false
        ));        
    
        //adiciona o campo ao formulario
        $this->addElement($endereco);
        
        
        //monta o campo numero
        $numero =  new Zend_Form_Element_Text('numero', array(
        		'label' => 'Numero',
        		'placeholder' => 'Digite aqui o numero',
        		'class'   => 'form-control',
        		'required' => false
        ));
        
        //adiciona o campo ao formulario
        $this->addElement($numero);
        
        
        //monta o campo complemento
        $complemento =  new Zend_Form_Element_Text('complemento', array(
        		'label' => 'Complemento',
        		'placeholder' => 'Digite aqui o complemento',
        		'class'   => 'form-control',
        		'required' => false
        ));
        
        //adiciona o campo ao formulario
        $this->addElement($complemento);
        
        //monta o campo bairro
        $bairro =  new Zend_Form_Element_Text('bairro', array(
        		'label' => 'Bairro',
        		'placeholder' => 'Digite aqui o Bairro',
        		'class'   => 'form-control',
        		'required' => false
        ));
        
        //adiciona o campo ao formulario
        $this->addElement($bairro);
        
        //monta o campo cidade
        $cidade =  new Zend_Form_Element_Text('cidade', array(
        		'label' => 'Cidade',
        		'placeholder' => 'Digite aqui a Cidade',
        		'class'   => 'form-control',
        		'required' => false
        ));
        
        //adiciona o campo ao formulario
        $this->addElement($cidade);
        
        //monta o campo estado
        $estado = new Zend_Form_Element_Select('estado',array(
        		'label' => 'Estado',
        		'required' => true,
        		'class' => 'form-control',
        		'multiOptions' => array(
        				 "" => "Selecione um Estado",
        				'AC'=>'Acre',
        				'AL'=>'Alagoas',
        				'AP'=>'Amapá',
        				'AM'=>'Amazonas',
        				'BA'=>'Bahia',
        				'CE'=>'Ceará',
        				'DF'=>'Distrito Federal',
        				'ES'=>'Espírito Santo',
        				'GO'=>'Goiás',
        				'MA'=>'Maranhão',
        				'MT'=>'Mato Grosso',
        				'MS'=>'Mato Grosso do Sul',
        				'MG'=>'Minas Gerais',
        				'PA'=>'Pará',
        				'PB'=>'Paraíba',
        				'PR'=>'Paraná',
        				'PE'=>'Pernambuco',
        				'PI'=>'Piauí',
        				'RJ'=>'Rio de Janeiro',
        				'RN'=>'Rio Grande do Norte',
        				'RS'=>'Rio Grande do Sul',
        				'RO'=>'Rondônia',
        				'RR'=>'Roraima',
        				'SC'=>'Santa Catarina',
        				'SP'=>'São Paulo',
        				'SE'=>'Sergipe',
        				'TO'=>'Tocantins'
        		)
        ));
        
        //adiciona o campo ao formulario
        $this->addElement($estado);
        //faz a validacao
        $estado->addFilter(new Zend_Filter_Null());
        
        
        $submit = new Zend_Form_Element_Submit('enviar', array(
        	'label' => 'Salvar',
        	"class" => "btn btn-large btn-block btn-success"
        ));
        $this->addElement($submit);

       
    }

}
