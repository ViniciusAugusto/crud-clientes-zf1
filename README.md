## CRUD DE CLIENTES

Sistema para cadastro de clientes com as funcionalidades de listagem, adição/edição e remoção.

## Projeto

1ª tela: Listagem dos clientes cadastrados em grid, disponibilizar botões: Adicionar, editar e remover clientes.
2ª tela: Tela para adicionar/editar clientes.

## Instalação

1ª crie o banco de dados de acordo com o arquivo database.sql
2ª altere os dados de conexao do arquivo application/configs/application.ini nas linhas 14 a 17
2ª abre em seu navegador o arquivo index.php da pasta /public, Exemplo: http://localhost/crmall-crud-clientes/public


## Desenvolvedor

Vinicius Augusto Cunha
